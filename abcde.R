setwd('C:/Users/Piuu/Desktop/modelos')
# Inciso A
#500 empresas
#6 a�os
emp<- read.csv('stocks_exam.csv',header = T, stringsAsFactors = FALSE)
emp<- emp[,-1]
datos<- read.csv('liab_divs_exam.csv',header = T, stringsAsFactors = FALSE)
rownames(datos) <- datos$variables
Liabilitie <- as.numeric(datos["TotalLiabilities", -1])
CurrentL <- as.numeric(datos["CurrentLiabilities", -1])
LongL <- Liabilitie - CurrentL
last <- nrow(emp)
Dinicial<-  as.numeric(datos["Dividends", -1])
G <- as.numeric(datos["DividendsGrowth", -1])
C <- 0.03
R <- 0.01
x <- seq(from = 1, to = nrow(emp), by = round(nrow(emp)/6))
probabilities <- data.frame(matrix(data=NA, nrow = length(x)-1, ncol = ncol(emp)))
p <- c()
for(i in 1:500){
  L <- Liabilitie[i]
  CL <- CurrentL[i]
  LTL <- L -CL
  time <- (0.5*CL + 10 * LTL)/L
  D0 <- Dinicial[i]
  g <- G[i]
  c <- 0.03
  r <- 0.01
  aux <- 0
    for(j in seq(from = 1, to = nrow(emp), by = round(nrow(emp)/6))){
    incremento <- round(nrow(emp)/6)
    aux <- aux + 1 
    if(aux>6) next;
    if(emp == x[length(x)]){incremento <- nrow(emp)-j}
    E<- emp[j:(j+incremento), i]
    D<- 0
    I<- 0
    for (k in 1:floor(time)){
      D <- D + D0 * (1 + g)^k * exp(r *( time - k))
      I <- I + L * c * exp(r *( time - k))
    }
    ndays <- 252
    A <- E + L
    sigma_E <- sd(diff(log(E)))*sqrt(ndays)
    sigma_A <- sd(diff(log(A)))*sqrt(ndays)
    d_1 <- (log(A/(L + D + I)) + (r + sigma_A^2))/(sigma_A)
    d_2 <- d_1 - sigma_A
    k_1 <- (log(A/(I + D))+(r + sigma_A^2/2))/(sigma_A)
    k_2 <- k_1 - sigma_A
    phi <- function(x) pnorm(x)
    A_n <- (E + (L + D + I)*exp(-r)*phi(d_2) - (D + I)*exp(-r)*phi(k_2))/(phi(d_1)+(1-phi(k_1))*(D/(D+I)))
    ct <- 0
    while (sum((A-A_n)^2) > 0.007){
      sigma_A_n <- sd(diff(log(A_n)))*sqrt(ndays)
      d_1 <- (log(A_n/(L + I + D))+(r + sigma_A_n^2/2))/(sigma_A_n)
      d_2 <- d_1 - sigma_A_n
      k_1 <- (log(A_n/(I + D)) + (r + sigma_A_n^2/2))/(sigma_A_n)
      k_2 <- k_1 - sigma_A_n
      A <- A_n
      A_n <- (E + (L + D + I)*exp(-r)*phi(d_2) - (D + I)*exp(-r)*phi(k_2))/(phi(d_1)+(1-phi(k_1))*(D/(D+I)))
      if(any(is.na(A_n)) || any(is.nan(A_n)) == T || any(A_n) <= 0) break;
      if (ct > 1000) break;
      ct <- ct + 1
    }
    ndays <- time * 262
    sigma_A <- (sd(diff(log(A)))*sqrt(ndays))
    mu_A <- (mean(diff(log(A)))*ndays)
    lA_T <- rnorm(100000,log(A_n[length(A_n)]) - ( mu_A - sigma_A^2/2),sigma_A)
    p[aux] <- length(lA_T[lA_T < log(L + D + I)])/ length(lA_T)
  } 
  probabilities[, i] <- 1 - (1-p)^(1/time)  
  i=i+1
} 
#Probabilidad Default
default_prob <- probabilities
states<-matrix(0,nrow = 6, ncol = 500)
for(i in 1:500){
    for(j in 1:6){
      if (default_prob[j,i] < 0.0018){
      states[j,i] <- 'AAA'
    }
    else if (default_prob[j,i] >= 0.0018 & default_prob[j,i] < 0.0028){
      states[j,i] <- 'AA'
    }
    else if (default_prob[j,i] >= 0.0028 & default_prob[j,i] < .01){
      states[j,i] <- 'A'
    }
    else if (default_prob[j,i] >= .01 & default_prob[j,i] < .0211){
      states[j,i] <- 'BBB'
    }
    else if (default_prob[j,i] >= .0211 & default_prob[j,i] < .0882){
      states[j,i] <- 'BB'
    }
    else if (default_prob[j,i] >= .0882 & default_prob[j,i] < .1853){
      states[j,i] <- 'B'
    }
    else if (default_prob[j,i] >= .1853 & default_prob[j,i] < 1){
      states[j,i] <- 'CCC'
    }
    else{ states[j,i] <- 'Default'}
  }
}
states<-as.matrix(states)
#transicion
mtf<-matrix(0,8,8)
for(z in 1:ncol(states)){
  for(j in 2:nrow(states)){
    if(states[j-1,z]=="AAA" & states[j,z]=="AAA"){
      mtf[1,1]<- mtf[1,1]+1
    }
    if(states[j-1,z]=="AAA" & states[j,z]=="AA"){
      mtf[1,2]<- mtf[1,2]+1
    }
    if(states[j-1,z]=="AAA" & states[j,z]=="A"){
      mtf[1,3]<- mtf[1,3]+1
    }
    if(states[j-1,z]=="AAA" & states[j,z]=="BBB"){
      mtf[1,4]<- mtf[1,4]+1
    }
    if(states[j-1,z]=="AAA" & states[j,z]=="BB"){
      mtf[1,5]<- mtf[1,5]+1
    }
    if(states[j-1,z]=="AAA" & states[j,z]=="B"){
      mtf[1,6]<- mtf[1,6]+1
    }
    if(states[j-1,z]=="AAA" & states[j,z]=="CCC"){
      mtf[1,7]<- mtf[1,7]+1
    }
    if(states[j-1,z]=="AAA" & states[j,z]=="Defauzt"){
      mtf[1,8]<- mtf[1,8]+1
    }
    if(states[j-1,z]=="AA" & states[j,z]=="AAA"){
      mtf[2,1]<- mtf[2,1]+1
    }
    if(states[j-1,z]=="AA" & states[j,z]=="AA"){
      mtf[2,2]<- mtf[2,2]+1
    }
    if(states[j-1,z]=="AA" & states[j,z]=="A"){
      mtf[2,3]<- mtf[2,3]+1
    }
    if(states[j-1,z]=="AA" & states[j,z]=="BBB"){
      mtf[2,4]<- mtf[2,4]+1
    }
    if(states[j-1,z]=="AA" & states[j,z]=="BB"){
      mtf[2,5]<- mtf[2,5]+1
    }
    if(states[j-1,z]=="AA" & states[j,z]=="B"){
      mtf[2,6]<- mtf[2,6]+1
    }
    if(states[j-1,z]=="AA" & states[j,z]=="CCC"){
      mtf[2,7]<- mtf[2,7]+1
    }
    if(states[j-1,z]=="AA" & states[j,z]=="Defauzt"){
      mtf[2,8]<- mtf[2,8]+1
    }
    if(states[j-1,z]=="A" & states[j,z]=="AAA"){
      mtf[3,1]<- mtf[3,1]+1
    }
    if(states[j-1,z]=="A" & states[j,z]=="AA"){
      mtf[3,2]<- mtf[3,2]+1
    }
    if(states[j-1,z]=="A" & states[j,z]=="A"){
      mtf[3,3]<- mtf[3,3]+1
    }
    if(states[j-1,z]=="A" & states[j,z]=="BBB"){
      mtf[3,4]<- mtf[3,4]+1
    }
    if(states[j-1,z]=="A" & states[j,z]=="BB"){
      mtf[3,5]<- mtf[3,5]+1
    }
    if(states[j-1,z]=="A" & states[j,z]=="B"){
      mtf[3,6]<- mtf[3,6]+1
    }
    if(states[j-1,z]=="A" & states[j,z]=="CCC"){
      mtf[3,7]<- mtf[3,7]+1
    }
    if(states[j-1,z]=="A" & states[j,z]=="Defauzt"){
      mtf[3,8]<- mtf[3,8]+1
    }
    if(states[j-1,z]=="BBB" & states[j,z]=="AAA"){
      mtf[4,1]<- mtf[4,1]+1
    }
    if(states[j-1,z]=="BBB" & states[j,z]=="AA"){
      mtf[4,2]<- mtf[4,2]+1
    }
    if(states[j-1,z]=="BBB" & states[j,z]=="A"){
      mtf[4,3]<- mtf[4,3]+1
    }
    if(states[j-1,z]=="BBB" & states[j,z]=="BBB"){
      mtf[4,4]<- mtf[4,4]+1
    }
    if(states[j-1,z]=="BBB" & states[j,z]=="BB"){
      mtf[4,5]<- mtf[4,5]+1
    }
    if(states[j-1,z]=="BBB" & states[j,z]=="B"){
      mtf[4,6]<- mtf[4,6]+1
    }
    if(states[j-1,z]=="BBB" & states[j,z]=="CCC"){
      mtf[4,7]<- mtf[4,7]+1
    }
    if(states[j-1,z]=="BBB" & states[j,z]=="Defauzt"){
      mtf[4,8]<- mtf[4,8]+1
    }
    if(states[j-1,z]=="BB" & states[j,z]=="AAA"){
      mtf[5,1]<- mtf[5,1]+1
    }
    if(states[j-1,z]=="BB" & states[j,z]=="AA"){
      mtf[5,2]<- mtf[5,2]+1
    }
    if(states[j-1,z]=="BB" & states[j,z]=="A"){
      mtf[5,3]<- mtf[5,3]+1
    }
    if(states[j-1,z]=="BB" & states[j,z]=="BBB"){
      mtf[5,4]<- mtf[5,4]+1
    }
    if(states[j-1,z]=="BB" & states[j,z]=="BB"){
      mtf[5,5]<- mtf[5,5]+1
    }
    if(states[j-1,z]=="BB" & states[j,z]=="B"){
      mtf[5,6]<- mtf[5,6]+1
    }
    if(states[j-1,z]=="BB" & states[j,z]=="CCC"){
      mtf[5,7]<- mtf[5,7]+1
    }
    if(states[j-1,z]=="BB" & states[j,z]=="Defauzt"){
      mtf[5,8]<- mtf[5,8]+1
    }
    if(states[j-1,z]=="B" & states[j,z]=="AAA"){
      mtf[6,1]<- mtf[6,1]+1
    }
    if(states[j-1,z]=="B" & states[j,z]=="AA"){
      mtf[6,2]<- mtf[6,2]+1
    }
    if(states[j-1,z]=="B" & states[j,z]=="A"){
      mtf[6,3]<- mtf[6,3]+1
    }
    if(states[j-1,z]=="B" & states[j,z]=="BBB"){
      mtf[6,4]<- mtf[6,4]+1
    }
    if(states[j-1,z]=="B" & states[j,z]=="BB"){
      mtf[6,5]<- mtf[6,5]+1
    }
    if(states[j-1,z]=="B" & states[j,z]=="B"){
      mtf[6,6]<- mtf[6,6]+1
    }
    if(states[j-1,z]=="B" & states[j,z]=="CCC"){
      mtf[6,7]<- mtf[6,7]+1
    }
    if(states[j-1,z]=="B" & states[j,z]=="Defauzt"){
      mtf[6,8]<- mtf[6,8]+1
    }
    if(states[j-1,z]=="CCC" & states[j,z]=="AAA"){
      mtf[7,1]<- mtf[7,1]+1
    }
    if(states[j-1,z]=="CCC" & states[j,z]=="AA"){
      mtf[7,2]<- mtf[7,2]+1
    }
    if(states[j-1,z]=="CCC" & states[j,z]=="A"){
      mtf[7,3]<- mtf[7,3]+1
    }
    if(states[j-1,z]=="CCC" & states[j,z]=="BBB"){
      mtf[7,4]<- mtf[7,4]+1
    }
    if(states[j-1,z]=="CCC" & states[j,z]=="BB"){
      mtf[7,5]<- mtf[7,5]+1
    }
    if(states[j-1,z]=="CCC" & states[j,z]=="B"){
      mtf[7,6]<- mtf[7,6]+1
    }
    if(states[j-1,z]=="CCC" & states[j,z]=="CCC"){
      mtf[7,7]<- mtf[7,7]+1
    }
    if(states[j-1,z]=="CCC" & states[j,z]=="Defauzt"){
      mtf[7,8]<- mtf[7,8]+1
    }
  }
}
mtf[8,8]<-1
for(x in 1:nrow(mtf)){
  for(y in 1:ncol(mtf)){
    mtf[x,y]<- mtf[x,y]/sums[x]
  }
}

#Inciso D
mt<-read.csv('mtf.csv',header = T, stringsAsFactors = FALSE)
myrownames <- mt[,1]
mt2 <- mt[,2:9]
row.names(mt2) <- myrownames
mt2<-as.matrix(mt2, ncol=8, nrow=8)
emp<- read.csv('stocks_exam_test.csv',header = T, stringsAsFactors = FALSE)
emp<- emp[,-1]
datos<- read.csv('liab_divs_exam_test.csv',header = T, stringsAsFactors = FALSE)
rownames(datos) <- datos$variables
Liabilitie <- as.numeric(datos["TotalLiabilities", -1])
CurrentL <- as.numeric(datos["CurrentLiabilities", -1])
LongL <- Liabilitie - CurrentL
last <- nrow(emp)
Dinicial<-  as.numeric(datos["Dividends", -1])
G <- as.numeric(datos["DividendsGrowth", -1])
C <- 0.03
R <- 0.01
probabilities <-c()
p <- c()
for(i in 1:500){
  L <- Liabilitie[i]
  CL <- CurrentL[i]
  LTL <- L -CL
  time <- (0.5*CL + 10 * LTL)/L
  D0 <- Dinicial[i]
  g <- G[i]
  c <- 0.03
  r <- 0.01
  aux <- 0
  aux <- aux + 1 
  E <- emp[1:280, i]
  D <- 0
  I <- 0
  for (k in 1:floor(time)){
    D <- D + D0 * (1 + g)^k * exp(r *( time - k))
    I <- I + L * c * exp(r *( time - k))
  }
  ndays <- 252
  A <- E + L
  sigma_E <- sd(diff(log(E)))*sqrt(ndays)
  sigma_A <- sd(diff(log(A)))*sqrt(ndays)
  d_1 <- (log(A/(L + D + I)) + (r + sigma_A^2))/(sigma_A)
  d_2 <- d_1 - sigma_A
  k_1 <- (log(A/(I + D))+(r + sigma_A^2/2))/(sigma_A)
  k_2 <- k_1 - sigma_A
  phi <- function(x) pnorm(x)
  A_n <- (E + (L + D + I)*exp(-r)*phi(d_2) - (D + I)*exp(-r)*phi(k_2))/(phi(d_1)+(1-phi(k_1))*(D/(D+I)))
  ct <- 0
  while (sum((A-A_n)^2) > 0.007){
    sigma_A_n <- sd(diff(log(A_n)))*sqrt(ndays)
    d_1 <- (log(A_n/(L + I + D))+(r + sigma_A_n^2/2))/(sigma_A_n)
    d_2 <- d_1 - sigma_A_n
    k_1 <- (log(A_n/(I + D)) + (r + sigma_A_n^2/2))/(sigma_A_n)
    k_2 <- k_1 - sigma_A_n
    A <- A_n
    A_n <- (E + (L + D + I)*exp(-r)*phi(d_2) - (D + I)*exp(-r)*phi(k_2))/(phi(d_1)+(1-phi(k_1))*(D/(D+I)))
    if(any(is.na(A_n)) || any(is.nan(A_n)) == T || any(A_n) <= 0) break;
    if (ct > 1000) break;
    ct <- ct + 1
  }
  ndays <- time * 262
  sigma_A <- (sd(diff(log(A)))*sqrt(ndays))
  mu_A <- (mean(diff(log(A)))*ndays)
  lA_T <- rnorm(100000,log(A_n[length(A_n)]) - ( mu_A - sigma_A^2/2),sigma_A)
  p[aux] <- length(lA_T[lA_T < log(L + D + I)])/ length(lA_T)
  probabilities[i] <- 1 - (1-p)^(1/time)  
  i=i+1
} 
#Probabilidad Default
default_prob <- probabilities
states<-c()
for(i in 1:500){
  if( is.nan(default_prob[i]) | is.na(default_prob[i])){
    states[i] <- 'AAA'
  }
  else if (default_prob[i] < 0.0018){
    states[i] <- 'AAA'
  }
  else if (default_prob[i] >= 0.0018 & default_prob[i] < 0.0028){
    states[i] <- 'AA'
  }
  else if (default_prob[i] >= 0.0028 & default_prob[i] < .01){
    states[i] <- 'A'
  }
  else if (default_prob[i] >= .01 & default_prob[i] < .0211){
    states[i] <- 'BBB'
  }
  else if (default_prob[i] >= .0211 & default_prob[i] < .0882){
    states[i] <- 'BB'
  }
  else if (default_prob[i] >= .0882 & default_prob[i] < .1853){
    states[i] <- 'B'
  }
  else if (default_prob[i] >= .1853 & default_prob[i] < 1){
    states[i] <- 'CCC'
  }
  else{ states[i] <- 'Default'}
}
states<-as.matrix(states)
c<-c("AAA", "AA", "A", "BBB", "BB", "B", "CCC", "Default")
totalstates<-matrix(0, ncol=8, nrow = 1)
for(a in 1:8){
  totalstates[a]<-length(states[states==c[a]])
}
year<-matrix(0,ncol=8,nrow=5)
year[1,]<-totalstates %*% mt2
for(b in 2:5){
  year[b,]<-year[b-1,] %*% mt2
}
year<-year/500

#Incieso E
#INCISO E

default_sample<-c()

for(k in 1:50){
  newstates<-sample(states[,1], length(states[,1]),replace=T)
  c<-c("AAA", "AA", "A", "BBB", "BB", "B", "CCC", "Default")
  totalstates_sample<-matrix(0, ncol=8, nrow = 1)
  for(a in 1:8){
    totalstates_sample[a]<-length(newstates[newstates==c[a]])
  }
  
  year_sample<-matrix(0,ncol=8,nrow=5)
  year_sample[1,]<-totalstates_sample %*% mt2
  
  for(b in 2:5){
    year_sample[b,]<-year_sample[b-1,] %*% mt2
  }
  year_sample<-year_sample/500
  default_sample<-cbind(default_sample,year_sample[,8])
  
}

means<-c()
desv<-c()
for(m in 1:5){
  means[m]<- mean(default_sample[m,])
  desv[m]<- sd(default_sample[m,])
}
